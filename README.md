# Digital Ocean Droplet and OpenVPN Setup Script

This script automates the creation of a Digital Ocean droplet and the installation of OpenVPN on it. It will also download the `client.ovpn` configuration file to your local machine.

## Prerequisites

Before running the script, ensure you have the following:

1. **Digital Ocean Account**: Create an account at [Digital Ocean](https://www.digitalocean.com/).
2. **Digital Ocean CLI (doctl)**: Install and configure `doctl` by following the [official guide](https://docs.digitalocean.com/reference/doctl/how-to/install/).
3. **SSH Key**: Ensure you have an SSH key added to your Digital Ocean account and available locally.
4. **Python 3**: Ensure you have Python 3 installed on your local machine.
5. **Python Packages**: Install the required Python packages using pip:

    ```bash
    pip install paramiko
    ```

## Configuration

Before running the script, you need to configure a few parts of it:

1. **Environment Variables**: Set the following environment variables in your `.zshrc` (or `.bashrc` for bash users) to avoid hardcoding sensitive information in the script.

    ```bash
    # DigitalOcean SSH Key ID
    export DO_SSH_KEY_ID=your-ssh-key-id

    # Path to your SSH private key
    export DO_SSH_KEY_FILE=/path/to/your/private/key

    # Optional: SSH username (default is 'root')
    export DO_SSH_USERNAME=root

    # Instance region (default is 'nyc1')
    export DO_INSTANCE_REGION=nyc1
    ```

    After updating the `.zshrc` or `.bashrc`, reload it by running `source ~/.zshrc` or `source ~/.bashrc`.

2. **Retrieve SSH Key ID**: You can get your SSH key ID by running:

    ```bash
    doctl compute ssh-key list
    ```

## Usage

1. **Clone the Repository**: Clone this repository to your local machine.

    ```bash
    git clone <repository-url>
    cd <repository-directory>
    ```

2. **Run the Script**: Execute the script to create the droplet and set up OpenVPN.

    ```bash
    python build-and-vpn.py
    ```

3. **Follow Prompts**: The script will prompt you to provide the instance name, size, image, and region. After the instance is created, it will ask if you want to proceed with the OpenVPN installation.

## Script Details

The script performs the following steps:

1. **Create a Digital Ocean Droplet**: Uses the provided instance name, size, image, and region to create a new droplet.
2. **Wait for Startup**: Waits for 30 seconds to allow the system to start up fully.
3. **SSH Connection**: Connects to the newly created droplet via SSH using the provided private key.
4. **Install OpenVPN**: Installs OpenVPN using the package manager and runs the OpenVPN installation script.
5. **Download `client.ovpn`**: Downloads the `client.ovpn` file from the droplet to your local machine.

## Example Configuration

Here is an example configuration for the script:

- **Instance Name**: `my-vpn-server`
- **Instance Size**: `s-1vcpu-1gb`
- **Instance Image**: `ubuntu-20-04-x64`
- **Instance Region**: `nyc1`
- **SSH Key ID**: `12345678`
- **SSH Private Key Path**: `/home/user/.ssh/id_rsa`

Make sure to replace the placeholders with your actual configuration values.

## Additional Notes

- **Public IP Extraction**: The script automatically extracts the public IP address of the created droplet.
- **OpenVPN Setup**: The script uses the `AUTO_INSTALL=y` flag to run the OpenVPN installation script headlessly.
- **Logging**: The script provides detailed logging to help diagnose any issues that occur during execution.

## Troubleshooting

- **SSH Issues**: Ensure your SSH key is correctly configured and the path is correctly set in the environment variables.
- **Digital Ocean API Errors**: Verify your API token and SSH key ID if you encounter errors related to droplet creation.
- **Upgrade Process Stuck**: If the `sudo apt-get upgrade -y` command seems stuck, it may be due to slow network conditions or a large number of packages to upgrade. You can manually SSH into the droplet to check the status of the upgrade process.
