import subprocess
import time
import paramiko
import re
import os

def run_command(command):
    """Run a shell command and return its output as a string."""
    result = subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return result.stdout.decode().strip()

def get_instance_name():
    return input("What should the new instance be named? ")

def get_instance_size():
    sizes = run_command('doctl compute size list')
    print("Available sizes:")
    print(sizes)
    return input("What size should the new instance be? ")

def get_instance_image():
    images = run_command('doctl compute image list-distribution')
    print("Available images:")
    print(images)
    return input("What image should be used? ")

def get_instance_region():
    regions = run_command('doctl compute region list')
    print("Available regions:")
    print(regions)
    return input("Which region should the new instance be created in? ")

def create_instance(name, size, image, region):
    # Read the SSH key ID from the environment variable
    ssh_key_id = os.getenv('DO_SSH_KEY_ID')
    if not ssh_key_id:
        raise ValueError("SSH key ID not found in the environment variable DO_SSH_KEY_ID")
    command = f'doctl compute droplet create {name} --size {size} --image {image} --region {region} --ssh-keys {ssh_key_id} --wait --format ID,Name,PublicIPv4'
    output = run_command(command)
    print("Instance created successfully:")
    print(output)
    # Extract the Public IPv4 from the output
    match = re.search(r'\b(?:\d{1,3}\.){3}\d{1,3}\b', output)
    if match:
        public_ip = match.group(0)
        return public_ip
    else:
        raise ValueError("Public IP not found in the output")

def ssh_connect(ip, username, key_file):
    """Connect to the server using SSH and return the SSH client."""
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, username=username, key_filename=key_file)
    return ssh

def run_ssh_command(ssh, command):
    """Run a command on the server via SSH."""
    stdin, stdout, stderr = ssh.exec_command(command)
    return stdout.read().decode(), stderr.read().decode()

#         Removing this command for now because it's getting hung during spin up "sudo apt-get upgrade -y",
def setup_openvpn(ssh):
    """Install OpenVPN using package manager and then run the installation script."""
    commands = [
        "sudo apt-get update",
        "sudo apt-get install -y openvpn",
        "curl -O https://raw.githubusercontent.com/angristan/openvpn-install/master/openvpn-install.sh",
        "chmod +x openvpn-install.sh",
        "AUTO_INSTALL=y ./openvpn-install.sh"
    ]
    for command in commands:
        print(f"Executing command: {command}")
        stdout, stderr = run_ssh_command(ssh, command)
        print(f"Command output:")
        print(stdout)
        if stderr:
            print(f"Command error:")
            print(stderr)

def main():
    try:
        name_of_instance = get_instance_name()
        size_of_instance = get_instance_size()
        instance_image = get_instance_image()
        instance_region = get_instance_region()
        public_ip = create_instance(name_of_instance, size_of_instance, instance_image, instance_region)
        
        print(f"Droplet Public IP: {public_ip}")

        # Wait for a while to ensure the server is up and running
        time.sleep(30)
        
        proceed = input("Do you want to proceed with the OpenVPN installation? (yes/no): ").strip().lower()
        if proceed == 'yes':
            print("Waiting for 45 seconds to allow the system to start up fully...")
            time.sleep(45)
            
            # SSH credentials
            username = os.getenv('DO_SSH_USERNAME', 'root')
            key_file = os.getenv('DO_SSH_KEY_FILE')  # Path to your SSH private key
            if not key_file:
                raise ValueError("SSH key file path not found in the environment variable DO_SSH_KEY_FILE")

            # Connect to the new droplet
            print(f"Connecting to {public_ip} via SSH...")
            ssh = ssh_connect(public_ip, username, key_file)
            print(f"Connected to {public_ip}")

            # Set up OpenVPN
            setup_openvpn(ssh)

            # Prompt user for the desired filename
            client_filename = input("Enter the desired name for your OpenVPN config file (e.g., my-vpn.ovpn): ").strip()
            if not client_filename.endswith('.ovpn'):
                client_filename += '.ovpn'

            # Set paths for the client config file
            local_client_path = os.path.join(os.path.expanduser("~"), client_filename)  # Save to root directory
            remote_client_path = "/root/client.ovpn"
            
            scp = paramiko.SFTPClient.from_transport(ssh.get_transport())
            scp.get(remote_client_path, local_client_path)
            print(f"\nOpenVPN config file has been downloaded to:")
            print(f"  {local_client_path}")
            print("\nYou can use this file to connect to your VPN using an OpenVPN client.")

            # Close the SSH connection
            ssh.close()
        else:
            print("Skipping OpenVPN installation. Droplet created successfully.")

    except subprocess.CalledProcessError as e:
        print(f"An error occurred: {e.stderr.decode().strip()}")
    except paramiko.SSHException as e:
        print(f"SSH error occurred: {str(e)}")
    except ValueError as e:
        print(f"Error: {str(e)}")

if __name__ == "__main__":
    main()

